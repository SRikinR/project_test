steps

# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permissions to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab CI user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start


#Command to register runner
sudo gitlab-runner register

#then copy paste the URL and registration token provode by the gitlab (settings>CI/CD>runners>expand)

#following commands and there effect on gitlab repo:
sudo gitlab-runner stop  --> pipeline will be paused [checkout pipeline status at (CI/CD>pipeline)]
sudo gitlab-runner start --> pipeline will be passed and ready to take jobs 

#after "sudo gitlab-runner start", if your pipeline status gets failed then write the below command in your terminal:
sudo rm /home/gitlab-runner/.bash_logout

#now after this try to push some commits to the repo and refresh the pipeline... status will be changed  to "passed" and will start running efficently.


