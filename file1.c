#include<stdio.h>
#include<stdlib.h>

struct myArray{
	int total_size;
	int used_size;
	int *ptr;
};

void createArray(struct myArray *a, int tsize, int usize){
	a->total_size=tsize;
	a->used_size=usize;
	a->ptr=(int *)malloc(tsize* sizeof(int));
}

void setVal(struct myArray *a){
	int i;
	for(i=0; i<a->used_size; i++){
		scanf("%d", &a[i]);
	}
}

void priVal(struct myArray *a){
	int i;
	for(i=0; i<a->used_size; i++){
		printf("%d ", a[i]);
	}
	printf("\n");
}

int main(){
	struct myArray rikin; 
	//int arr[100]={1,2,3,4,5); no need of this rn.
	
	printf("Creating an Array...\n");
	createArray(&rikin, 100, 2);
	printf("Array Created!!\n");

	printf("Let's now set the value:\n");
	setVal(&rikin);

	printf("Value you added were:\n");
	priVal(&rikin);

	return 0;
}
